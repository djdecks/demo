#!/bin/bash

if [ "$#" -eq 1 ]; then
	echo "Budowanie pakietu przy użyciu meaven\n"
	mvn clean package
	
	echo "Tworzenie obrazu docker'a\n"
	docker build -f src/main/docker/Dockerfile -t testdemo . 
	
	echo "Uruchamianie usługi z wykorzystanie docker'a\n"
	docker run -e my_params=$1 -p 8080:8080 testdemo

else
	echo "Użycie skryptu ./script.sh link, gdzie argument #link jest linkiem do pliku .yaml znajdującego się w repozytorium github'a.\n"
fi

