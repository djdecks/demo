package com.example.demo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.http.HttpServlet;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;

import com.example.demo.controll.TaskHandler;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
	 @Autowired
     private TaskHandler controller;
	 
	 private MockMvc mockMvc;
	 
	 @Before
     public void setup() {
         this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
     }

	@Test
	public void contextLoads() {
		try {
			mockMvc.perform(get("/")).andExpect(status().isOk()).
				andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).
				andExpect(jsonPath("$.[0].id").value("3142"));
			
			        
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void check(){
		try {
			mockMvc.perform(get("/engines?pressure_threshold=71&temp_threshold=80")).andExpect(status().isOk()).
			andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).
			andExpect(jsonPath("").value("123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
