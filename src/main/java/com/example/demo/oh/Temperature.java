package com.example.demo.oh;

public class Temperature {
	private int id;
	private int master_sensor_id;
	private String type;
	private int value;
	private int min_value;
	private int max_value;

	public Temperature(int id, int master_sensor_id, String type, int value, int min_value, int max_value) {
		super();
		this.id = id;
		this.master_sensor_id = master_sensor_id;
		this.type = type;
		this.value = value;
		this.min_value = min_value;
		this.max_value = max_value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMaster_sensor_id() {
		return master_sensor_id;
	}

	public void setMaster_sensor_id(int master_sensor_id) {
		this.master_sensor_id = master_sensor_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getMin_value() {
		return min_value;
	}

	public void setMin_value(int min_value) {
		this.min_value = min_value;
	}

	public int getMax_value() {
		return max_value;
	}

	public void setMax_value(int max_value) {
		this.max_value = max_value;
	}

	@Override
	public String toString() {
		return "id: " + getId() + System.getProperty("line.separator") + "master-sensor-id: " + getMaster_sensor_id()
				+ System.getProperty("line.separator") + "type: " + getType() + System.getProperty("line.separator")
				+ "value: " + getValue() + System.getProperty("line.separator") + "min_value: " + getMin_value()
				+ System.getProperty("line.separator") + "max_value: " + getMax_value()
				+ System.getProperty("line.separator");

	}

}
