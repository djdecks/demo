package com.example.demo.oh;

import java.util.ArrayList;

public class Engine {

	private int id;
	private String engine;
	private String location;
	private String type;
	private String name;
	private int value;
	private int min_value;
	private int max_value;
	private ArrayList<Temperature> temperature_sensors = new ArrayList<>();

	public Engine(int id, String engine, String location, String type, String name, int value, int min_value,
			int max_value) {
		super();
		this.id = id;
		this.engine = engine;
		this.type = type;
		this.name = name;
		this.value = value;
		this.min_value = min_value;
		this.max_value = max_value;

	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public ArrayList<Temperature> getTemperature_sensors() {
		return temperature_sensors;
	}

	public void setTemperature_sensors(ArrayList<Temperature> temperature_sensors) {
		this.temperature_sensors = temperature_sensors;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public void setMin_value(int min_value) {
		this.min_value = min_value;
	}

	public void setMax_value(int max_value) {
		this.max_value = max_value;
	}

	public int getId() {
		return id;
	}

	public String getEngine() {
		return engine;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}

	public int getMin_value() {
		return min_value;
	}

	public int getMax_value() {
		return max_value;
	}

	@Override
	public String toString() {
		return "id: " + getId() + System.getProperty("line.separator") + "engine: " + getEngine()
				+ System.getProperty("line.separator") + "location: " + getLocation()
				+ System.getProperty("line.separator") + "type: " + getType() + System.getProperty("line.separator")
				+ "name: " + getName() + System.getProperty("line.separator") + "value: " + getValue()
				+ System.getProperty("line.separator") + "min_value: " + getMin_value()
				+ System.getProperty("line.separator") + "max_value: " + getMax_value()
				+ System.getProperty("line.separator") + "lista czujników temperatury: " + getTemperature_sensors();
	}

	public boolean addTemperature_sensor(Temperature temperature) {
		if (temperature instanceof Temperature) {
			getTemperature_sensors().add(temperature);
			return true;
		} else {
			return false;
		}

	}

	public boolean setValueForSensor(Changer changer, int ids) {

		if ((this.getId() == ids) && changer.getOperation()) {

			if (this.getMax_value() >= (this.getValue() + changer.getValue())) {
				this.setValue(this.getValue() + changer.getValue());
				return true;
			} else {
				return false;
			}
		} else if ((this.getId() == ids) && !changer.getOperation()) {

			if (this.getMin_value() <= (this.getValue() - changer.getValue())) {
				this.setValue(this.getValue() - changer.getValue());
				return true;
			} else {
				return false;
			}
		} else {
			for (Temperature t : this.getTemperature_sensors()) {
				if (changer.getOperation()) {
					if ((t.getId() == ids) && (t.getMax_value() >= (t.getValue() + changer.getValue()))) {
						t.setValue(t.getValue() + changer.getValue());
						return true;
					}

				} else if (!changer.getOperation()) {
					if ((t.getId() == ids) && (t.getMin_value() <= (t.getValue() - changer.getValue()))) {
						t.setValue(t.getValue() - changer.getValue());
						return true;
					}
				}
			}
		}

		return false;
	}

}
