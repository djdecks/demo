package com.example.demo.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Tools {

	public static void downloadFile() {

		String link = System.getProperty("url");
		if (link.contains("blob")) {
			link = urlConvertToRaw(link);
		}

		String fileName = "sensors.yml";
		URL url = null;
		try {
			url = new URL(link);
		} catch (MalformedURLException e4) {
			System.err.println("Błędny adres URL");			
			e4.printStackTrace();
		}
		HttpURLConnection http = null;
		try {
			http = (HttpURLConnection) url.openConnection();
		} catch (IOException e4) {
			System.err.println("Błąd podczas otwierania połączenia z rządanym URL");
			e4.printStackTrace();
		}

		InputStream input = null;
		try {
			input = http.getInputStream();
		} catch (IOException e3) {
			System.err.println("Błąd podczas otwierania pliku z danymi w formacie .yaml");
			e3.printStackTrace();
		}
		byte[] buffer = new byte[4096];
		int n = -1;
		OutputStream output = null;
		File file = new File(fileName);
		try {
			output = new FileOutputStream(file);
		} catch (FileNotFoundException e2) {
			System.err.println("Nie odnaleziono wskazanego pliku na dysku");
			e2.printStackTrace();
		}
		try {
			while ((n = input.read(buffer)) != -1) {
				output.write(buffer, 0, n);
			}
		} catch (IOException e2) {
			System.err.println("Wystąpił błąd podczas zapisu do pliku");
			e2.printStackTrace();
		}
		try {
			output.close();
		} catch (IOException e2) {
			System.err.println("Wystąpił błąd podczas zamykania pliku");
			e2.printStackTrace();
		}
	}

	private static String urlConvertToRaw(String url) {
		String[] copy = url.split("/");

		StringBuilder sts = new StringBuilder();
		sts.append("https://raw.githubusercontent.com/");

		for (int i = 3; i < copy.length; ++i) {
			if (!copy[i].equals("blob")) {
				sts.append(copy[i]);
				if (i < copy.length - 1) {
					sts.append("/");
				}
			}
		}
		return sts.toString();
	}
	
	
}
