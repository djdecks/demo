package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.tools.Tools;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {	
		
		if (args.length == 1) {			
			System.setProperty("url", args[0]);			
			Tools.downloadFile();
			SpringApplication.run(DemoApplication.class, args);
		} else {
			System.err.println("Wykonanie programu: ./scrip.sh link");
			System.exit(-1);
		}
	}
}
