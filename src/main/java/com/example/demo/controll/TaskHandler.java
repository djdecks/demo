package com.example.demo.controll;

import java.io.FileNotFoundException;

import java.io.FileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.example.demo.oh.Changer;
import com.example.demo.oh.Engine;
import com.example.demo.oh.Temperature;

@RestController
public class TaskHandler {

	private ArrayList<Engine> list_of_engines = new ArrayList<>();

	@PostConstruct
	public void afterInit() {

		String filePath = "sensors.yml";
		YamlReader reader = null;
		try {

			reader = new YamlReader(new FileReader(filePath));
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		Object object = null;
		try {
			object = reader.read();
		} catch (YamlException e1) {
			System.err.println("Wystąpił problem podczas odczytu pliku .yaml");
			e1.printStackTrace();
		}

		@SuppressWarnings("unchecked")
		ArrayList<HashMap<String, String>> array = (ArrayList<HashMap<String, String>>) object;

		Engine engine_Obj_temp = null;

		for (HashMap<String, String> element : array) {

			if (element.size() > 6) {
				engine_Obj_temp = new Engine(Integer.parseInt(element.get("id")), element.get("engine"),
						element.get("location"), element.get("type"), element.get("name"),
						Integer.parseInt(element.get("value")), Integer.parseInt(element.get("min_value")),
						Integer.parseInt(element.get("max_value")));

				list_of_engines.add(engine_Obj_temp);

			} else {
				Temperature temp_sensor = new Temperature(Integer.parseInt(element.get("id")),
						Integer.parseInt(element.get("master-sensor-id")), element.get("type"),
						Integer.parseInt(element.get("value")), Integer.parseInt(element.get("min_value")),
						Integer.parseInt(element.get("max_value")));
				for (Engine engine : list_of_engines) {
					if (engine.getId() == temp_sensor.getMaster_sensor_id()) {
						engine.addTemperature_sensor(temp_sensor);
					}
				}
			}
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/", produces = "application/json")
	public @ResponseBody ArrayList<Engine> listOfEngines() {
		return list_of_engines;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/engines")
	public @ResponseBody ResponseEntity<List<String>> listOfIncorrectlyOperatingEngines(
			@RequestParam(value = "pressure_threshold") int pressure_min,
			@RequestParam(value = "temp_threshold") int temp_max) {

		return JSONlistOfIncorrectlyOperatingEngines(pressure_min, temp_max);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/sensors/{id}")
	public String update(@RequestBody Changer changer, @PathVariable("id") int id) {

		boolean isSetted = false;
		for (Engine e : list_of_engines) {
			if (e.setValueForSensor(changer, id) == true) {
				isSetted = true;
				break;
			}
		}
		if (isSetted) {
			return "Zmiana wartości czujnika przebiegła pomyślnie";
		} else {
			return "Zmiana wartości czujnika przebiegła niepomyślnie";
		}

	}

	private ResponseEntity<List<String>> JSONlistOfIncorrectlyOperatingEngines(int pressure, int temperature) {

		List<String> LOIOE = new ArrayList<String>();
		for (Engine engine : list_of_engines) {
			if (engine.getValue() < pressure) {

				if (!LOIOE.contains(engine.getEngine())) {
					for (Temperature temp : engine.getTemperature_sensors()) {
						if (temp.getValue() > temperature) {
							if (!LOIOE.contains(engine.getEngine())) {
								LOIOE.add(engine.getEngine());
							}
						}

					}
				}
			}

		}
		//String json = new Gson().toJson(LOIOE);
		return new ResponseEntity<List<String>>(LOIOE, HttpStatus.OK);
		//return json;
	}

}
